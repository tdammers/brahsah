module Render
    ( renderTT
    , extractLinksTT
    )
where

import Text.HTML.TagSoup
import Text.HTML.TagSoup.Tree
import Control.Monad
import Control.Applicative ( (<$>) )
import Control.Monad.State
import Control.Monad.Writer
import qualified Data.Map as Map
import Data.Maybe
import Data.Char (toLower)
import Tags
import Trim

-- | This is our main rendering function. It takes a parsed document and
-- returns a textual representation and an indexed list of links as they appear
-- in the document.
runTT :: [TagTree String] -> (String, [(Int, String)])
runTT tt = runRender $ mapM_ treeR tt

renderTT :: [TagTree String] -> String
renderTT = fst . runTT

extractLinksTT :: [TagTree String] -> [(Int, String)]
extractLinksTT = snd . runTT

-- | Data structure to track various state as we walk the document
data RenderState =
    RenderState
        { rsLinkURLs :: [String]
        , rsAtBlockStart :: Bool
        }

defRenderState :: RenderState
defRenderState =
    RenderState
        { rsLinkURLs = []
        , rsAtBlockStart = True
        }

-- | The monad stack we'll use while rendering. The Writer part collects the
-- rendered text, while the State part carries the RenderState.
type Render = WriterT String (State RenderState)

runRender :: Render a -> (String, [(Int, String)])
runRender r =
    let r' = r >> gets rsLinkURLs
        (urls, txt) = evalState (runWriterT r') defRenderState
        urlMap = zip [1..] urls
    in (trim txt, urlMap)

-- | Write some inline text.
inline :: String -> Render ()
inline str = do
    modify (\s -> s { rsAtBlockStart = False })
    tell str

-- | Enforce that whatever gets written next is at a block boundary.
blockBoundary :: Render ()
blockBoundary = do
    atB <- gets rsAtBlockStart
    when (not atB) (tell "\n")
    modify (\s -> s { rsAtBlockStart = True })

-- | Push an URL onto the render state's link stack.
pushUrl :: String -> Render Int
pushUrl url = do
    modify (\s -> s { rsLinkURLs = rsLinkURLs s ++ [url] })
    length <$> gets rsLinkURLs

treeR :: TagTree String -> Render ()
treeR (TagBranch t attrs children) =
    tagR t (Map.fromList attrs) children
treeR (TagLeaf (TagText str)) = inline str
treeR (TagLeaf _) = return ()

tagR :: String -> Map.Map String String -> [TagTree String] -> Render ()
tagR "br" _ _ = blockBoundary
tagR "hr" _ _ = blockBoundary >> inline "------------" >> blockBoundary
tagR "a" attrs inner = do
    let url = fromMaybe "" $ Map.lookup "href" attrs
    mapM_ treeR inner
    when (not $ null url) $ do
            index <- pushUrl url
            inline $ "[" ++ show index ++ "]"
tagR "img" attrs _ = do
    let url = fromMaybe "" $ Map.lookup "src" attrs
        alt = fromMaybe "" $ Map.lookup "alt" attrs
    index <- pushUrl url
    inline "((img:"
    inline alt
    inline "["
    inline $ show index
    inline "]))"
tagR "input" attrs _ = do
    let inputType = fromMaybe "" $ Map.lookup "type" attrs
        name = fromMaybe "?" $ Map.lookup "name" attrs
        checked = isJust $ Map.lookup "checked" attrs
        value = fromMaybe "" $ Map.lookup "value" attrs
    case map toLower inputType of
            "text" -> inline " ________ "
            "password" -> inline " ******** "
            "checkbox" -> inline "[" >> inline (if checked then "X" else " ") >> inline "] "
            "radio" -> inline "(" >> inline (if checked then "o" else " ") >> inline ") "
            "submit" -> inline "<" >> inline value >> inline ">"
            _ -> return ()
    inline $ "(" ++ name ++ ")"

tagR t attrs inner
    | isInvisibleTag t = return ()
    | isBlockTag t = do
            blockBoundary
            mapM_ treeR inner
            blockBoundary
    | otherwise = mapM_ treeR inner
