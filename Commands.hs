{-#LANGUAGE TypeSynonymInstances, FlexibleInstances #-}
module Commands
    ( runInput
    )
where

import System.IO
import System.Exit
import Text.Parsec
import Browser
import Control.Monad
import Control.Monad.IO.Class
import Control.Applicative ( (<$>) )
import Data.Map as Map
import Data.List
import Control.Monad.Error

runInput :: Handle -> Browser ()
runInput h = do
    liftIO $ do
        hSetBuffering h NoBuffering
        hSetEcho h False
    go ""
    where
        go :: String -> Browser ()
        go src = do
            liftIO $ whenM (hIsEOF h) exitSuccess
            c <- liftIO $ hGetChar h
            let src' = src ++ [c]
            let r = runParser keystrokesP h "<input>" src'
            either
                (const $ go src')
                handled_
                r

keystrokesP = try cancelledKeysP
            <|> try quitKeysP
            <|> genericKeysP
            <|> cmdKeysP

cancelledKeysP = do
    many (noneOf "\ESC")
    char '\ESC'
    return $ return ()

quitKeysP = do
    string "zz" <|> try (many (noneOf "\EOT") >> string "\EOT")
    return $ liftIO exitSuccess

data KeyCmd = SimpleKey String (Browser ()) | RepeatableKey String (Int -> Browser ())

keyCmds :: [KeyCmd]
keyCmds =
    [ RepeatableKey "h" historyBack
    , RepeatableKey "l" historyFwd
    ]

mkKeyCmdP (SimpleKey k a) = try $ do
    string k
    return a
mkKeyCmdP (RepeatableKey k a) = try $ do
    n <- option 1 (try intP)
    string k
    return $ a n

genericKeysP =
    foldl1 (<|>) $ Data.List.map mkKeyCmdP keyCmds

cmdKeysP = do
    many $ noneOf ":"
    char ':'
    h <- getState
    return $ runCommandInput h

runCommandInput :: Handle -> Browser ()
runCommandInput h = do
    ln <- liftIO $ do
            hSetBuffering h LineBuffering
            hSetEcho h True
            hPutStr stderr ":"
            hGetLine h
    let r = runParser commandP h "<command>" ln
    either (\_ -> throwError (UserInputError "Invalid command" ln)) handled_ r

type CommandF = [String] -> Browser ()

class Command c where
    runC :: c -> CommandF

-- zero arguments
instance Command (Browser a) where
    runC a [] = a >> return ()
    runC a xs = a >> liftIO (putStrLn "Warning: excess parameters") >> return ()

instance Command (IO a) where
    runC a [] = liftIO a >> return ()
    runC a xs = liftIO (a >> putStrLn "Warning: excess parameters") >> return ()

-- required argument
instance Command c => Command (String -> c) where
    runC a (x:xs) = runC (a x) xs
    runC a [] = throwError NotEnoughParametersError

-- optional argument
instance Command c => Command (Maybe String -> c) where
    runC a [] = runC (a Nothing) []
    runC a (x:xs) = runC (a $ Just x) xs

data CommandDef =
    CommandDef
        { cdefTokens :: [String]
        , cdefCommand :: CommandF
        , cdefDescription :: String
        , cdefArgDescription :: [String]
        }

simpleCommands :: [CommandDef]
simpleCommands =
        [ CommandDef ["quit", "q"]       (runC exitSuccess) "quit" []
        , CommandDef ["links", "ls"]     (runC renderLinkList) "list available links on current page" []
        , CommandDef ["go", "open", "o"] (runC gotoUrlS) "go to URL" ["URL"]
        , CommandDef ["reload", "r"]     (runC reload) "reload the current URL" []
        , CommandDef ["hist"]            (runC $ showHistory) "show history" []
        , CommandDef ["help"]            (runC $ listCommands simpleCommands) "show this help" []
        ]

listCommands :: [CommandDef] -> IO ()
listCommands = mapM_ listCommand

listCommand :: CommandDef -> IO ()
listCommand (CommandDef tokens _ desc argDescs) = do
    putStr . concat . intersperse " " . Data.List.map ((':':) . (++ argsString)) $ tokens
    putStr " - "
    putStrLn desc
    where
        argsString =
            concat $
            Data.List.map ((" {" ++) . (++ "}")) argDescs

commandP = try simpleCommandP
        <|> try numberCmdP

simpleCommandP = do
    cmd <- many1 alphaNum
    args <- many $ try (many1 space >> cmdArgP)
    eof
    let a = findSimpleCommandAction cmd
    maybe (fail "No such command") (return . ($ args)) a

cmdArgP = try quotedString <|> word

quotedString = do
    delim <- oneOf "'\""
    inner <- many (noneOf [delim, '\\'] <|> escapedChar)
    char delim
    return inner

escapedChar = do
    char '\\'
    c <- anyChar
    case c of
        'n' -> return '\n'
        't' -> return '\t'
        'b' -> return '\b'
        'r' -> return '\r'
        otherwise -> return c

word = many1 $ noneOf "'\" \t\b\r\n"

findSimpleCommandAction cmd =
    let p = elem cmd . cdefTokens
    in cdefCommand <$> find p simpleCommands

numberCmdP = do
    i <- intP
    return $ followLinkByIndex i >> render

whenM c a = c >>= flip when a

intP = read <$> many1 digit
