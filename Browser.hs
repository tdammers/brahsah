{-#LANGUAGE TemplateHaskell, TupleSections, StandaloneDeriving #-}
module Browser
where

import Control.Monad
import Control.Monad.State
import Control.Monad.Error
import Control.Monad.IO.Class
import Control.Applicative ( (<$>) )
import Data.List
import Data.Char
import Data.Monoid
import Data.Maybe
import System.Environment
import System.IO
import Network.Curl
import Render
import Parse
import Trim
import Safe
import URL
import Text.HTML.TagSoup
import Text.HTML.TagSoup.Tree
import Control.Lens

deriving instance Ord CurlCode

data BrowserError = BrowserOK
                  | MalformedURL String
                  | CurlError CurlCode
                  | UserInputError String String
                  | NotEnoughParametersError
                  | OtherBrowserError String
                  | UnspecifiedBrowserError
        deriving (Show, Ord, Eq)

instance Error BrowserError where
    noMsg = UnspecifiedBrowserError
    strMsg = OtherBrowserError

isBrowserError :: BrowserError -> Bool
isBrowserError = (/= BrowserOK)

instance Monoid BrowserError where
    mempty = BrowserOK
    mappend x BrowserOK = x
    mappend _ x = x

data BrowserState =
    BrowserState
        { _bsCurrentUrl :: URL
        , _bsHistory :: [URL]
        , _bsHistoryPos :: Int 
        , _bsDocument :: [TagTree String]
        }

$(makeLenses ''BrowserState)

defBrowserState :: BrowserState
defBrowserState =
    BrowserState
        { _bsCurrentUrl = FileURL []
        , _bsHistory = []
        , _bsHistoryPos = 0
        , _bsDocument = []
        }

type Browser = ErrorT BrowserError (StateT BrowserState IO)

runBrowser :: Browser a -> IO (Either BrowserError a)
runBrowser b =
    evalStateT (runErrorT b) defBrowserState

handled :: a -> Browser a -> Browser a
handled a b = catchError b $ handleBrowserError a

handled_ = handled ()

handleBrowserError :: a -> BrowserError -> Browser a
handleBrowserError x err = do
    liftIO $ putStrLn $ show err
    return x

pushHistory :: URL -> Browser ()
pushHistory url = do
    histPos <- use bsHistoryPos
    bsHistory %= (url:) . (drop histPos)
    bsHistoryPos .= 0

showHistory :: Browser ()
showHistory = do
    hist <- use bsHistory
    histPos <- use bsHistoryPos
    mapM_ (showHistLine histPos) (zip [0..] hist)
    where
        showHistLine currentPos (pos, url) = liftIO $ do
            putStr $ if currentPos == pos
                        then "* "
                        else "  "
            putStrLn $ writeURL url

render :: Browser ()
render = do
    use bsDocument >>= liftIO . putStrLn . renderTT
    use bsCurrentUrl >>= liftIO . putStrLn . writeURL

renderLinkList :: Browser ()
renderLinkList =
    getLinks >>= renderLinks

renderLinks :: [(Int, URL)] -> Browser ()
renderLinks [] =
    liftIO $ putStrLn "Nothing to see here, move on people..."
renderLinks ll = do
    liftIO $ mapM_ writeLink ll
    where
        writeLink (i,u) = putStrLn $
            "[" <> show i <> "]: " <> writeURL u

loadUrlS :: String -> Browser ()
loadUrlS url = maybe (throwError $ MalformedURL url) loadUrl $ parseURL url

loadUrl :: URL -> Browser ()
loadUrl url = do
    baseUrl <- use bsCurrentUrl
    let url' = baseUrl <> url
    liftIO $ putStrLn ("loading " ++ writeURL url' ++ "...")
    loadUrl' url'

loadUrl' :: URL -> Browser ()
loadUrl' (OtherURL "about" "brahsah") = do
    let body = "<h1>Brahsah</h1><p>Copyright (c) 2013 Tobias Dammers.</p>"
    bsDocument .= parse body
loadUrl' (OtherURL "system" "stdin") = do
    body <- liftIO getContents
    bsDocument .= parse body
loadUrl' (FileURL path) = do
    let fn = concat . intersperse "/" $ path
    body <- liftIO $ openFile fn ReadMode >>= hGetContents
    bsCurrentUrl .= FileURL path
    bsDocument .= parse body
loadUrl' url = do
    let urlS = writeURL url
    resp <- liftIO (curlGetResponse_ urlS [CurlFollowLocation True] :: IO CurlResponse)
    let code = respCurlCode resp
        body = respBody resp
    if code == CurlOK
        then do
            effUrlI <- liftIO $ respGetInfo resp EffectiveUrl
            let IString effUrlS = effUrlI
            effUrl <- maybe (throwError $ MalformedURL effUrlS) return $ parseURL effUrlS
            bsCurrentUrl .= effUrl
            bsDocument .= parse body
        else
            throwError $ CurlError code

getLinks :: Browser [(Int, URL)]
getLinks = parseLinks . extractLinksTT <$> use bsDocument

parseLinks :: [(a, String)] -> [(a, URL)]
parseLinks xs = catMaybes $ map parseLink xs

parseLink :: (a, String) -> Maybe (a, URL)
parseLink (k, v) = do
    parseURL v >>= return . (k,)

getLink :: Int -> Browser URL
getLink i = do
    links <- getLinks
    let founds = map snd . filter ((== i) . fst) $ links
    if null founds
        then (error "Invalid link index")
        else (return $ head founds)

followLinkByIndex :: Int -> Browser ()
followLinkByIndex i = do
    url <- getLink i
    gotoUrl url

gotoUrl :: URL -> Browser ()
gotoUrl url = do
    loadUrl url
    use bsCurrentUrl >>= pushHistory
    render

gotoUrlS :: String -> Browser ()
gotoUrlS url = do
    loadUrlS url
    use bsCurrentUrl >>= pushHistory
    render

gotoHist :: Browser ()
gotoHist = do
    histPos <- use bsHistoryPos
    hist <- use bsHistory
    when (length hist > histPos) $ do
        let url = head $ drop histPos hist
        loadUrl url
        render

historyBack :: Int -> Browser ()
historyBack n = do
    hist <- use bsHistory
    histPos <- use bsHistoryPos
    let n' = min n (length hist - histPos - 1)
    when (n' > 0) $ do
        bsHistoryPos += n'
        gotoHist

historyFwd :: Int -> Browser ()
historyFwd n = do
    hist <- use bsHistory
    histPos <- use bsHistoryPos
    let n' = min n histPos
    when (n' > 0) $ do
        bsHistoryPos -= n'
        gotoHist

reload :: Browser ()
reload = use bsCurrentUrl >>= gotoUrl
