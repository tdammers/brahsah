{-#LANGUAGE TupleSections #-}
import Control.Monad
import Control.Applicative ( (<$>) )
import Data.List
import Data.Char
import Data.Monoid ( (<>) )
import Data.Maybe
import System.Environment
import System.IO
import Network.Curl
import Render
import Parse
import Trim
import Safe
import URL
import Browser
import Commands
import Control.Monad.IO.Class

runStdin :: Browser ()
runStdin = do
    loadUrlS "system:stdin"
    render

runUrl :: String -> Browser ()
runUrl url = do
    handled_ $ gotoUrlS url
    go
    where
        go = do
            runInput stdin
            isTTY <- liftIO $ hIsTerminalDevice stdin
            when isTTY go


followLink baseURL links linkID =
    let linkURL = fromMaybe baseURL (lookup linkID links)
    in runUrl $ baseURL <> linkURL

main = withCurlDo $ do
    fn <- fromMaybe "about:brahsah" . headMay <$> getArgs
    let ac = if fn == "-" then runStdin else runUrl fn
    runBrowser ac
