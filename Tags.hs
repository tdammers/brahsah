module Tags
    ( isBlockTag
    , isInvisibleTag
    )
where

isBlockTag :: String -> Bool
isBlockTag = (`elem` blockTags)

blockTags = [
        "html", "style", "body",
        "div", "table", "tr", "td", "th",
        "p", "br", "hr", "li",
        "dd", "dl", "dh",
        "pre", "blockquote",
        "textarea"
    ] ++
    [ ['h', i] | i <- ['1'..'6'] ]

isInvisibleTag :: String -> Bool
isInvisibleTag = (`elem` invisibleTags)

invisibleTags = [
        "script", "style", "head"
    ]

