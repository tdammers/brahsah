module URL
    ( parseURL, writeURL
    , URL (..)
    , Domain, Path, Parameters
    , urlJoin
    )
where

import Text.Parsec
import Control.Monad
import Safe
import Data.Monoid
import Data.List

type Domain = [String]
type Path = [String]
type Parameters = [(String, String)]

data URL = HttpURL HttpProtocol Domain Path Parameters
         | FileURL Path
         | OtherURL String String
         | RelativeURL Path Parameters
    deriving (Show, Eq, Ord)

instance Monoid URL where
    mempty = RelativeURL [] []
    mappend = urlJoin

data HttpProtocol = Http | Https
    deriving (Show, Eq, Ord, Enum)

urlJoin :: URL -> URL -> URL
urlJoin
    (HttpURL proto domain origPath origParams)
    (RelativeURL newPath newParams)
    = HttpURL proto domain (joinPaths origPath newPath) newParams
urlJoin
    (FileURL origPath)
    (RelativeURL newPath _)
    = FileURL (joinPaths origPath newPath)
urlJoin _ a = a

joinPaths :: Path -> Path -> Path
-- anything onto empty path
joinPaths [] p = p
-- empty path onto anything
joinPaths p [] = p
-- absolute onto absolute
joinPaths ("":_) ("":p) = "":p
-- absolute onto relative
joinPaths _ ("":p) = p
-- relative onto anything non-empty
joinPaths p q = init p ++ q

writeURL :: URL -> String
writeURL (HttpURL proto domain path params) =
    mconcat $
        [ if proto == Http then "http" else "https"
        , "://"
        , intercalate "." domain
        ]
        ++
        (
            if null path
                then []
                else [ "/"
                     , intercalate "/" $ dropWhile null path
                     ]
        )
        ++
        if null params
            then []
            else [ "?"
                 , writeParams params
                 ]
writeURL (FileURL path) =
    mconcat $
        [ "file://"
        , intercalate "/" path
        ]
writeURL (OtherURL proto remainder) =
    mconcat $
        [ proto
        , ":"
        , remainder
        ]
writeURL (RelativeURL path params) =
    mconcat $
        [ intercalate "/" path
        ]
        ++
        if null params
            then []
            else [ "?"
                 , writeParams params
                 ]

writeParams params =
    intercalate "&" $ map writePair params
    where
        writePair (k,"") = k
        writePair (k, v) = k ++ "=" ++ v

parseURL :: String -> Maybe URL
parseURL url =
    let result = runParser urlP () "URL" url
    in either (const Nothing) Just result

urlP = httpsUrlP
    <|> httpUrlP
    <|> fileUrlP
    <|> otherUrlP
    <|> relativeUrlP
    <?> "URL"

protocolNameP pname = try $ do
    string pname
    string "://"
    return ()

httpsUrlP = do
    protocolNameP "https"
    httpTailP Https

httpUrlP = do
    protocolNameP "http"
    httpTailP Http

fileUrlP = do
    protocolNameP "file"
    path <- pathP
    return $ FileURL path

otherUrlP = try $ do
    protocolName <- many1 (oneOf ['a'..'z'])
    string ":"
    tailStr <- many1 anyChar
    return $ OtherURL protocolName tailStr

relativeUrlP = try $ do
    (path, params) <- httpPathP
    return $ RelativeURL path params

pathP = (liftM2 (:)) pathPart (many $ char '/' >> pathPart)

pathPart = many (noneOf nonPathChars)
nonPathChars = "/?"

httpTailP protocol = do
    domain <- (liftM2 (:)) domainPart (many (char '.' >> domainPart))
    (path, params) <- httpPathP
    return $ HttpURL protocol domain (tailSafe path) params
    where
        domainPart = many (oneOf domainChars)
        domainChars = ['a'..'z'] ++ ['0'..'9'] ++ "-_"

httpPathP = do
    path <- option [] pathP
    params <- option [] $ do
        char '?'
        paramList
    return (path, params)

paramList =
    (liftM2 (:)) param (many $ char '&' >> param)
    where
        paramWord = many1 (noneOf "&=")
        param = do
            left <- paramWord
            right <- option "" (char '=' >> paramWord)
            return (left, right)
