module Trim
    ( trim, rtrim, ltrim )
where

import Data.Char
import Data.List

ltrim = dropWhile isSpace
rtrim = dropWhileEnd isSpace
trim = rtrim . ltrim
