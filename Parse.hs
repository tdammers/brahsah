module Parse
where

import Text.HTML.TagSoup
import Text.HTML.TagSoup.Tree
import Data.List
import Data.Char
import Data.Maybe
import Safe
import Trim
import Tags

parse = tagTree . collapseWhitespace . parseTags . killScriptTags

killScriptTags :: String -> String
killScriptTags html
	| null html = []
	| "<script" `isPrefixOf` html = killScriptRemainder $ dropWhile (/= '>') html
	| otherwise = head html:killScriptTags (tail html)
	where
		killScriptRemainder :: String -> String
		killScriptRemainder html
			| null html = []
			| "</script>" `isPrefixOf` html = killScriptTags . tailSafe . dropWhile (/= '>') $ html
			| otherwise = killScriptRemainder . tailSafe $ html

collapseWhitespace :: [Tag String] -> [Tag String]
collapseWhitespace =
    filter isNeeded . go
    where 
        isNeeded (TagText "") = False
        isNeeded _ = True
        go [] = []
        go (TagText str:[]) = [TagText (rtrim str)]
        go (TagText str:TagOpen t a:xs) =
            let str' = if isBlockTag t then rtrim str else str
            in (TagText str':go (TagOpen t a:xs))
        go (TagText str:TagClose t:xs) =
            let str' = if isBlockTag t then rtrim str else str
            in (TagText str':go (TagClose t:xs))
        go (TagOpen t a:TagText str:xs) =
            let str' = if isBlockTag t then ltrim str else str
            in (TagOpen t a:go (TagText str':xs))
        go (TagClose t:TagText str:xs) =
            let str' = if isBlockTag t then ltrim str else str
            in (TagClose t:go (TagText str':xs))
        go (x:xs) = x:go xs


